package md.utm.pr.labs;

import java.util.concurrent.CountDownLatch;

import md.utm.pr.labs.sync.DoubleTailThread;
import md.utm.pr.labs.sync.DoubleWaitThread;
import md.utm.pr.labs.sync.HeadThread;
import md.utm.pr.labs.sync.MiddleThread;

public class TaskTen {
	public static void main(String[] args) {
		CountDownLatch headLatch = new CountDownLatch(1);
		CountDownLatch firstLatch = new CountDownLatch(1);
		CountDownLatch fourthLatch = new CountDownLatch(1);
		CountDownLatch secondLatch = new CountDownLatch(1);
		CountDownLatch sixthLatch = new CountDownLatch(1);
		new HeadThread(headLatch, "Thread-3").start();
		new MiddleThread(headLatch, firstLatch, "Thread-1").start();
		new DoubleWaitThread(headLatch, firstLatch, secondLatch, "Thread-2").start();
		new DoubleWaitThread(headLatch, secondLatch, fourthLatch, "Thread-5").start();
		new DoubleWaitThread(headLatch, fourthLatch, sixthLatch, "Thread-4").start();
		new DoubleTailThread(headLatch, sixthLatch, "Thread-6").start();
	}
}
