package md.utm.pr.labs;

import java.util.concurrent.CountDownLatch;

import md.utm.pr.labs.sync.HeadThread;
import md.utm.pr.labs.sync.SinkThread;

public class TaskNine {
	public static void main(String[] args) {
		CountDownLatch firthLatch = new CountDownLatch(1);
		CountDownLatch secondLatch = new CountDownLatch(1);
		CountDownLatch thirdLatch = new CountDownLatch(1);
		CountDownLatch fourthLatch = new CountDownLatch(1);
		new HeadThread(firthLatch, "Thread-1").start();
		new HeadThread(secondLatch, "Thread-2").start();
		new HeadThread(thirdLatch, "Thread-3").start();
		new HeadThread(fourthLatch, "Thread-4").start();
		new SinkThread("Thread-5", firthLatch, secondLatch, thirdLatch).start();
		new SinkThread("Thread-6", secondLatch, thirdLatch, fourthLatch).start();
	}
}
