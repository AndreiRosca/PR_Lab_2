package md.utm.pr.labs;

import java.util.concurrent.CountDownLatch;

import md.utm.pr.labs.sync.HeadThread;
import md.utm.pr.labs.sync.MiddleThread;
import md.utm.pr.labs.sync.TailThread;

public class TaskSeven {
	public static void main(String[] args) {
		CountDownLatch headLatch = new CountDownLatch(1);
		CountDownLatch topMiddle = new CountDownLatch(1);
		CountDownLatch bottomMiddle = new CountDownLatch(1);
		new HeadThread(headLatch, "Thread-1").start();
		new MiddleThread(headLatch, topMiddle, "Thread-2").start();
		new MiddleThread(headLatch, bottomMiddle, "Thread-3").start();
		new TailThread(topMiddle, "Thread-4").start();
		new TailThread(topMiddle, "Thread-5").start();
		new TailThread(bottomMiddle, "Thread-6").start();
		new TailThread(bottomMiddle, "Thread-7").start();
	}
}
