package md.utm.pr.labs;

import java.util.concurrent.CountDownLatch;

import md.utm.pr.labs.sync.HeadThread;
import md.utm.pr.labs.sync.MiddleThread;
import md.utm.pr.labs.sync.TailThread;

public class TaskEight {
	public static void main(String[] args) {
		CountDownLatch headTop = new CountDownLatch(2);
		CountDownLatch headBottom = new CountDownLatch(2);
		CountDownLatch tailLatch = new CountDownLatch(2);
		new HeadThread(headTop, "Thread-1").start();
		new HeadThread(headTop, "Thread-2").start();
		new HeadThread(headBottom, "Thread-3").start();
		new HeadThread(headBottom, "Thread-4").start();
		new MiddleThread(headTop, tailLatch, "Thread-5").start();
		new MiddleThread(headBottom, tailLatch, "Thread-6").start();
		new TailThread(tailLatch, "Thread-7").start();
	}
}
