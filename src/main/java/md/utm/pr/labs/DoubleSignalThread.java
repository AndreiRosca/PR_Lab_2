package md.utm.pr.labs;

import java.util.concurrent.CountDownLatch;

public class DoubleSignalThread extends Thread {
	private final CountDownLatch firstSignalLatch;
	private final CountDownLatch secondSignalLatch;

	public DoubleSignalThread(CountDownLatch firstSignalLatch, CountDownLatch secondSignalLatch, String name) {
		this.firstSignalLatch = firstSignalLatch;
		this.secondSignalLatch = secondSignalLatch;
		setName(name);
	}

	public void run() {
		System.out.println(Thread.currentThread().getName() + " waiting for the first signal...");
		try {
			firstSignalLatch.await();
			System.out.println(Thread.currentThread().getName() + " received the first signal...");
			System.out.println(Thread.currentThread().getName() + " waiting for the second signal...");
			secondSignalLatch.await();
			System.out.println(Thread.currentThread().getName() + " received the second signal...");
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}
}
