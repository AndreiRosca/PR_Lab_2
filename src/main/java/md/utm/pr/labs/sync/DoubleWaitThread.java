package md.utm.pr.labs.sync;

import java.util.concurrent.CountDownLatch;

public class DoubleWaitThread extends Thread {
	private final CountDownLatch firstWaitLatch;
	private final CountDownLatch secondWaitLatch;
	private final CountDownLatch signalLatch;

	public DoubleWaitThread(CountDownLatch firstWaitLatch, CountDownLatch secondWaitLatch, CountDownLatch signalLatch,
			String name) {
		this.firstWaitLatch = firstWaitLatch;
		this.secondWaitLatch = secondWaitLatch;
		this.signalLatch = signalLatch;
		setName(name);
	}

	public void run() {
		System.out.println(Thread.currentThread().getName() + " waiting for the first signal...");
		try {
			firstWaitLatch.await();
			System.out.println(Thread.currentThread().getName() + " received the first signal...");
			System.out.println(Thread.currentThread().getName() + " waiting for the second signal...");
			secondWaitLatch.await();
			System.out.println(Thread.currentThread().getName() + " received the second signal...");
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		System.out.println(Thread.currentThread().getName() + " sending the signal...");
		signalLatch.countDown();
		System.out.println(Thread.currentThread().getName() + " sent the signal...");
	}
}
