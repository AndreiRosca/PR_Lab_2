package md.utm.pr.labs.sync;

import java.util.concurrent.CountDownLatch;

public class TailThread extends Thread {
	private final CountDownLatch latch;
	
	public TailThread(CountDownLatch latch, String name) {
		this.latch = latch;
		setName(name);
	}
	
	public void run() {
		System.out.println(Thread.currentThread().getName() + " waiting for signal...");
		try {
			latch.await();
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		System.out.println(Thread.currentThread().getName() + " received the signal...");
	}
}