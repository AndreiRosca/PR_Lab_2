package md.utm.pr.labs.sync;

import java.util.concurrent.CountDownLatch;

public class HeadThread extends Thread {
	private final CountDownLatch latch;

	public HeadThread(CountDownLatch latch, String name) {
		this.latch = latch;
		setName(name);
	}

	public void run() {
		try {
			Thread.sleep(Math.round(Math.random() * 500));
			System.out.println(Thread.currentThread().getName() + " sending the signal...");
			latch.countDown();
			System.out.println(Thread.currentThread().getName() + " sent the signal...");
		} catch (InterruptedException e) {

		}
	}
}