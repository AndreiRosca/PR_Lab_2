package md.utm.pr.labs.sync;

import java.util.concurrent.CountDownLatch;

public class SinkThread extends Thread {
	private final CountDownLatch[] waitLatches;

	public SinkThread(String name, CountDownLatch ...waitLatches) {
		this.waitLatches = waitLatches;
		setName(name);
	}

	public void run() {
		try {
			Thread.sleep(Math.round(Math.random() * 500));
			for (int i = 0; i < waitLatches.length; ++i) {
				try {
					System.out.println(Thread.currentThread().getName() + 
							" waiting for the signal #" + (1 + i));
					CountDownLatch latch = waitLatches[i];
					latch.await();
					System.out.println(Thread.currentThread().getName() + 
							" received the signal #" + (1 + i));
					Thread.sleep(Math.round(Math.random() * 500));
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
			}
		} catch (InterruptedException e) {
			
		}
	}
}
