package md.utm.pr.labs.sync;

import java.util.concurrent.CountDownLatch;

public class DoubleTailThread extends Thread {
	private final CountDownLatch firstLatch;
	private CountDownLatch secondLatch;

	public DoubleTailThread(CountDownLatch firstlatch, CountDownLatch secondlatch, String name) {
		this.firstLatch = firstlatch;
		this.secondLatch = secondlatch;
		setName(name);
	}

	public void run() {
		System.out.println(Thread.currentThread().getName() + " waiting for the first signal...");
		try {
			firstLatch.await();
			System.out.println(Thread.currentThread().getName() + " received the first signal...");
			System.out.println(Thread.currentThread().getName() + " waiting for the second signal...");
			secondLatch.await();
			System.out.println(Thread.currentThread().getName() + " received the second signal...");
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}
}
